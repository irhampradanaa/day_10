SOAL 1


Microsoft Windows [Version 10.0.18362.1016]
(c) 2019 Microsoft Corporation. All rights reserved.

C:\WINDOWS\system32>cd..

C:\Windows>cd..

C:\>cd xampp\mysql\bin\

C:\xampp\mysql\bin>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 11
Server version: 10.4.11-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> use irham;
Database changed
MariaDB [irham]> create database Myshop;
Query OK, 1 row affected (0.003 sec)

MariaDB [irham]> use Myshop;
Database changed
MariaDB [Myshop]>



SOAL 2


MariaDB [Myshop]> create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.327 sec)
MariaDB [Myshop]> create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> Primary key (id)
    -> );
Query OK, 0 rows affected (0.291 sec)
MariaDB [Myshop]> create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> deskription varchar(255),
    -> Primary key (id)
    -> );
Query OK, 0 rows affected (0.205 sec)


SOAL 3


MariaDB [Myshop]> insert into categories(name)
    -> values ("gatget");
Query OK, 1 row affected (0.293 sec)

MariaDB [Myshop]> insert into users(name)
    -> values ("Irham");
Query OK, 1 row affected (0.071 sec)

MariaDB [Myshop]> insert into users(email)
    -> values ("irhampradanaaa@gmail.com");
Query OK, 1 row affected (0.052 sec)

MariaDB [Myshop]> insert into users(password)
    -> values ("pppp");
Query OK, 1 row affected (0.069 sec)


SOAL 4



MariaDB [Myshop]> select name from users;
+-------+
| name  |
+-------+
| Irham |
| NULL  |
| NULL  |
+-------+
3 rows in set (0.042 sec)

MariaDB [Myshop]> select name,email from users;
+-------+--------------------------+
| name  | email                    |
+-------+--------------------------+
| Irham | NULL                     |
| NULL  | irhampradanaaa@gmail.com |
| NULL  | NULL                     |
+-------+--------------------------+
3 rows in set (0.000 sec)

MariaDB [Myshop]> select * from users;
+----+-------+--------------------------+----------+
| id | name  | email                    | password |
+----+-------+--------------------------+----------+
|  1 | Irham | NULL                     | NULL     |
|  2 | NULL  | irhampradanaaa@gmail.com | NULL     |
|  3 | NULL  | NULL                     | pppp     |
+----+-------+--------------------------+----------+
3 rows in set (0.000 sec)

MariaDB [Myshop]>




SOAL 5



MariaDB [Myshop]> update users set name=Irhammm where name="Irham";
query OK, 1 row affected (0.138 sec)
Rows matched: 1 Changed: 1 Warning: 0

MariaDB [Myshop]>select * from users;
+----+---------+--------------------------+----------+
| id | name    | email                    | password |
+----+---------+--------------------------+----------+
|  1 | Irhammm | NULL                     | NULL     |
|  2 | NULL    | irhampradanaaa@gmail.com | NULL     |
|  3 | NULL    | NULL                     | pppp     |
+----+---------+--------------------------+----------+
3 rows in set (0.000 sec)
